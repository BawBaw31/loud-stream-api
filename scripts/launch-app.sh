#!/usr/bin/env bash

source /home/ubuntu/.profile
cd /home/ubuntu/app
pip install -r requirements.txt
alembic upgrade head
uvicorn sql_app.main:app --host 0.0.0.0