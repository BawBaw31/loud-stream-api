terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region                   = "eu-west-3" # Paris
  shared_credentials_files = [var.aws_cred_file]
  profile                  = "default"
}

module "network" {
  source = "./modules/network"
  subnet_tags = {
    "Name" = "Subnet"
  }
  vpc_tags = {
    "Name" = "VPC"
  }
}

module "s3_bucket" {
  source = "./modules/aws-s3"

  bucket_name = "loud-stream-bucket-terraform"

  tags = {
    Terraform   = "true"
    Environment = "production"
  }
}

module "db_instance" {
  source = "./modules/aws-db"

  db_name  = "loudStreamDb"
  username = var.db_username
  password = var.db_password
  network = {
    default_subnet_id = module.network.default_subnet_id
    other_subnet_id   = module.network.other_subnet_id
    security_groups   = [module.network.secgroup_rds_id]
  }
}

module "ec2_instance" {
  source = "./modules/aws-ec2"

  ec2_machine_image = "ami-0493936afbe820b28"
  ec2_instance_type = "t2.micro"

  network = {
    default_subnet_id = module.network.default_subnet_id
    security_groups   = [module.network.secgroup_allow_web_id]
  }

  ssh_keys                    = var.ssh_keys
  jwt_secret                  = var.jwt_secret
  jwt_algorithm               = var.jwt_algorithm
  access_token_expire_minutes = var.access_token_expire_minutes
  database_url                = "postgresql://${var.db_username}:${var.db_password}@${module.db_instance.host}:${module.db_instance.port}/${module.db_instance.db_name}"
  aws_bucket_name             = module.s3_bucket.bucket_name
}