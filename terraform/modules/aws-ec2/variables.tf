variable "ec2_machine_image" {
  type    = string
  default = "ami-0493936afbe820b28"
}

variable "ec2_instance_type" {
  type    = string
  default = "t2.micro"
}

variable "ssh_keys" {
  type = object({
    public_key  = string
    private_key = string
  })
}

variable "network" {
  type = object({
    default_subnet_id = string,
    security_groups = list(string)
  })
}

# ENV VARIABLES
variable "jwt_secret" {
  type = string
}

variable "jwt_algorithm" {
  type    = string
  default = "HS256"
}

variable "access_token_expire_minutes" {
  type    = number
  default = 30
}

variable "database_url" {
  type = string
}

variable "aws_bucket_name" {
  type = string
}