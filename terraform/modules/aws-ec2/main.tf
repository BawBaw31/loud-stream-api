resource "aws_iam_role" "s3_access_role" {
  name = "s3_access_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_instance_profile" "s3_access_profile" {
  name = "s3_access_profile"
  role = aws_iam_role.s3_access_role.name
}

resource "aws_iam_role_policy" "s3_access_policy" {
  name = "s3_access_policy"
  role = aws_iam_role.s3_access_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = var.ssh_keys.public_key
}

resource "aws_instance" "prod" {
  ami                  = var.ec2_machine_image
  instance_type        = var.ec2_instance_type
  iam_instance_profile = aws_iam_instance_profile.s3_access_profile.name
  key_name             = aws_key_pair.deployer.key_name
  vpc_security_group_ids = var.network.security_groups
  subnet_id = var.network.default_subnet_id

  user_data = <<EOF
#!/usr/bin/env bash

sudo apt-get -y update
sudo apt-get -y install python3-pip
sudo pip3 install --upgrade pip
EOF

  provisioner "file" {
    source      = "./alembic-ini.prod"
    destination = "/home/ubuntu/alembic.ini"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = var.ssh_keys.private_key
      host        = self.public_ip
    }
  }

  provisioner "remote-exec" {
    inline = [
      "echo JWT_SECRET_KEY=\"${var.jwt_secret}\" | sudo tee -a /etc/environment",
      "echo JWT_ALGORITHM=\"${var.jwt_algorithm}\" | sudo tee -a /etc/environment",
      "echo ACCESS_TOKEN_EXPIRE_MINUTES=\"${var.access_token_expire_minutes}\" | sudo tee -a /etc/environment",
      "echo DATABASE_URL=\"${var.database_url}\" | sudo tee -a /etc/environment",
      "echo AWS_BUCKET_NAME=\"${var.aws_bucket_name}\" | sudo tee -a /etc/environment",
      "sudo touch /etc/systemd/system/loud-stream.service",
      "mkdir /home/ubuntu/app",
      "sed -i 's|sqlalchemy.url =|sqlalchemy.url = ${var.database_url}|' /home/ubuntu/alembic.ini",
      "mv /home/ubuntu/alembic.ini /home/ubuntu/app",
      "echo [Unit] | sudo tee -a /etc/systemd/system/loud-stream.service",
      "echo Description=launch loud-stream app | sudo tee -a /etc/systemd/system/loud-stream.service",
      "echo | sudo tee -a /etc/systemd/system/loud-stream.service",
      "echo [Service] | sudo tee -a /etc/systemd/system/loud-stream.service",
      "echo EnvironmentFile=/etc/environment | sudo tee -a /etc/systemd/system/loud-stream.service",
      "echo ExecStart=/usr/bin/bash /home/ubuntu/app/scripts/launch-app.sh | sudo tee -a /etc/systemd/system/loud-stream.service",
      "echo User=ubuntu | sudo tee -a /etc/systemd/system/loud-stream.service",
      "echo | sudo tee -a /etc/systemd/system/loud-stream.service",
      "echo [Install] | sudo tee -a /etc/systemd/system/loud-stream.service",
      "echo WantedBy=shutdown.target | sudo tee -a /etc/systemd/system/loud-stream.service",
      "sudo systemctl daemon-reload",
      "sudo systemctl enable loud-stream.service",
    ]

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = var.ssh_keys.private_key
      host        = self.public_ip
    }
  }

  tags = {
    Name = "loud-stream-api-terraform"
  }
}
