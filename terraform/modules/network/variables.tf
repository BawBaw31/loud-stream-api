variable "vpc_tags" {
  description = "Tags to set on the vpc."
  type = map(string)
  default = {}
}

variable "subnet_tags" {
  description = "Tags to set on the subnet."
  type = map(string)
  default = {}
}