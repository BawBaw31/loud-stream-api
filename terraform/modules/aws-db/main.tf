resource "aws_db_subnet_group" "default" {
  name       = "main"
  subnet_ids = [var.network.default_subnet_id, var.network.other_subnet_id]

  tags = {
    Name = "RDS subnet group"
  }
}

resource "aws_db_instance" "postgres" {
  allocated_storage    = 10
  db_name              = var.db_name
  vpc_security_group_ids = var.network.security_groups
  db_subnet_group_name = aws_db_subnet_group.default.id
  engine               = "postgres"
  instance_class       = "db.t3.micro"
  username             = var.username
  password             = var.password
  skip_final_snapshot  = true
}