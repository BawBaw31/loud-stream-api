variable "db_name" {
  type = string
}

variable "username" {
    description = "Username for the master DB user."
    type        = string
}

variable "password" {
    description = "Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file."
    type        = string
}

variable "network" {
  type = object({
    default_subnet_id = string,
    other_subnet_id = string,
    security_groups = list(string)
  })
}