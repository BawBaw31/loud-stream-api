variable "aws_cred_file" {
  type    = string
  default = "~/.aws/credentials"
}

variable "ssh_keys" {
  type = object({
    public_key  = string
    private_key = string
  })
}

variable "db_username" {
  type      = string
  sensitive = true
}

variable "db_password" {
  type      = string
  sensitive = true
}

# EC2 ENV VARIABLES
variable "jwt_secret" {
  type = string
}

variable "jwt_algorithm" {
  type    = string
  default = "HS256"
}

variable "access_token_expire_minutes" {
  type    = number
  default = 30
}