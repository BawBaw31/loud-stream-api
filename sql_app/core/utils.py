from fastapi import HTTPException, UploadFile, status
from mutagen import mp3, wave


async def check_music_files_types(audio_file: UploadFile, cover_file: UploadFile):
    errors = {}
    if audio_file.content_type != "audio/mpeg" and audio_file.content_type != "audio/wav":
        errors["audio_file"] = "Audio file must be mp3 or wav"
    if cover_file.content_type != "image/jpeg" and cover_file.content_type != "image/png":
        errors["cover_file"] = "Cover file must be jpeg or png"

    if len(errors.keys()):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=errors)


async def check_album_cover_file_type(cover_file: UploadFile):
    errors = {}
    if cover_file.content_type != "image/jpeg" and cover_file.content_type != "image/png":
        errors["cover_file"] = "Cover file must be jpeg or png"
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=errors)


def get_total_track_time(audio_file: UploadFile) -> int:
    audio: mp3.MP3 or wave.WAVE
    try:
        if audio_file.content_type == "audio/mpeg":
            audio = mp3.MP3(audio_file.file)
            return audio.info.length
        if audio_file.content_type == "audio/wav":
            audio = wave.WAVE(audio_file.file)
            return audio.info.length
        else:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail="Audio file must be mp3 or wav")
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))
    finally:
        audio.save(audio_file.file)
