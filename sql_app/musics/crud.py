import datetime

from fastapi import HTTPException, status
from sqlalchemy.orm import Session

from ..albums.crud import get_album
from . import models, schemas


def get_music(db: Session, music_id: int) -> schemas.Music:
    return db.query(models.Music).filter(models.Music.id == music_id).first()


def get_published_music(db: Session, music_id: int) -> schemas.Music:
    return db.query(models.Music).filter(models.Music.id == music_id, models.Music.published == True).first()


def get_unpublished_music(db: Session, music_id: int, owner_id: int) -> schemas.Music:
    return db.query(models.Music).filter(models.Music.id == music_id, models.Music.published == False, models.Music.owner_id == owner_id).first()


def get_musics(db: Session, skip: int = 0, limit: int = 100) -> list[schemas.Music]:
    return db.query(models.Music).offset(skip).limit(limit).all()


def get_latest_musics(db: Session, skip: int = 0, limit: int = 100) -> list[schemas.Music]:
    return db.query(models.Music).filter(models.Music.published == True).order_by(models.Music.published_date.desc()).offset(skip).limit(limit).all()


def get_musics_by_query(db: Session, query: str, limit: int = 8) -> list[schemas.Music]:
    return db.query(models.Music).filter(models.Music.published == True, models.Music.title.like(f"%{query}%")).order_by(models.Music.published_date.desc()).limit(limit).all()


def create_music(db: Session, music_order: schemas.MusicOrder, owner_id: int) -> schemas.Music:
    db_music = models.Music(title=music_order.title, genre=music_order.genre._value_,
                            audio_file_name=music_order.audio_file_name, cover_file_name=music_order.cover_file_name,
                            owner_id=owner_id, total_track_time=music_order.total_track_time)

    db.add(db_music)
    db.commit()
    db.refresh(db_music)

    return db_music


def add_music_to_album(db: Session, music_id: int, album_id: int) -> schemas.Music:
    db_music = get_music(db, music_id)
    db_album = get_album(db, album_id)

    if db_music.published | db_album.published:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Music or album already published")

    db_music.album = album_id

    db.commit()
    db.refresh(db_music)

    return db_music


def publish_music(db: Session, music_id: int, current_artist_id: int) -> schemas.Music:
    db_music = get_music(db, music_id)

    if db_music.owner.id != current_artist_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="You are not allowed to publish this music")

    if db_music.published:
        print(db_music)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Music already published")

    db_music.published = True
    db_music.published_date = datetime.datetime.utcnow()

    db.commit()
    db.refresh(db_music)

    return db_music


def get_artist_musics(db: Session, artist_id: int, published: bool = True, skip: int = 0, limit: int = 100) -> list[schemas.Music]:
    return db.query(models.Music).filter(models.Music.published == published, models.Music.owner_id == artist_id).order_by(models.Music.published_date.desc()).offset(skip).limit(limit).all()
