import os
import pathlib


given_wrong_file_path = os.path.join(pathlib.Path(
    __file__).parent.absolute(), "files", "artist_audio_music.mp3")

given_cover_file_path = os.path.join(pathlib.Path(
    __file__).parent.absolute(), "files", "artist_cover_album.jpg")

given_album_cover_file = {"cover_file": ("artist_cover_album.jpg", open(
    given_cover_file_path, "rb"), "image/jpeg")}

given_wrong_album_cover_file = {"cover_file": ("artist_audio_music.mp3", open(given_wrong_file_path, "rb"), "audio/mpeg")}

given_album = {"album_title": "album", "album_genre": "Pop"}
