import os
import pathlib

given_audio_file_path = os.path.join(pathlib.Path(
    __file__).parent.absolute(), "files", "artist_audio_music.mp3")

given_cover_file_path = os.path.join(pathlib.Path(
    __file__).parent.absolute(), "files", "artist_cover_music.jpg")

given_files = {"audio_file": ["artist_audio_music.mp3", given_audio_file_path, "audio/mpeg"], "cover_file": [
    "artist_cover_music.jpg", given_cover_file_path, "image/jpeg"]}

given_wrong_files = {"cover_file": ["artist_audio_music.mp3", given_audio_file_path, "audio/mpeg"], "audio_file": [
    "artist_cover_music.jpg", given_cover_file_path, "image/jpeg"]}

given_music = {"music_title": "music", "music_genre": "Pop"}
given_music_2 = {"music_title": "music 2", "music_genre": "Pop"}
given_music_3 = {"music_title": "music 3", "music_genre": "Pop"}
