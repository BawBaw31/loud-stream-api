
from io import BufferedReader

from fastapi import UploadFile
from fastapi.testclient import TestClient

from .artists import authenticate_util


def seed_music(client: TestClient, given_artist: dict[str, str],
               given_music: dict[str, str], given_files: dict[str, list[str]], with_token=False):
    token = authenticate_util(client, given_artist)
    file_handles: list[BufferedReader] = []
    try:
        files = {}
        i = 0
        for key, file in given_files.items():
            file_handle = open(file[1], "rb")
            file_handles.append(file_handle)
            files[key] = (file[0], file_handles[i], file[2])
            i += 1

        music = client.post(
            "/musics/", headers={"Authorization": f"Bearer {token}"}, data=given_music, files=files)

        if with_token:
            return music.json(), token

        return music.json()
    finally:
        for file in file_handles:
            file.close()


def seed_published_music(client: TestClient, given_artist: dict[str, str],
                         given_music: dict[str, str], given_files: dict[str, UploadFile], with_token=False):
    music, token = seed_music(client, given_artist, given_music,
                              given_files, with_token=True)
    music_id = music["id"]
    published_music = client.put(
        f"/musics/{music_id}/publish", headers={"Authorization": f"Bearer {token}"})

    if with_token:
        return published_music.json(), token

    return published_music.json()
