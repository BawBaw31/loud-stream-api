import asyncio
from io import BufferedReader

import pytest
from fastapi.testclient import TestClient

from ..core.database import get_db
from ..core.storage import Storage
from ..main import app
from .fixtures.artists import given_artist, given_artist_2, given_artist_3
from .fixtures.musics import (given_files, given_music, given_music_2,
                              given_music_3, given_wrong_files)
from .utils.artists import authenticate_util
# test_db is used to override the get_db dependency
from .utils.db import override_get_db, test_db
from .utils.musics import seed_music, seed_published_music
from .utils.storage import OverrideStorage

app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[Storage] = OverrideStorage

client = TestClient(app)


@pytest.mark.usefixtures("test_db")
def test_add_music():
    file_handles: list[BufferedReader] = []
    try:
        # Given
        files = {}
        i = 0
        for key, file in given_files.items():
            file_handle = open(file[1], "rb")
            file_handles.append(file_handle)
            files[key] = (file[0], file_handles[i], file[2])
            i += 1
        token = authenticate_util(client, given_artist)

        # When
        response = client.post(
            "/musics/", headers={"Authorization": f"Bearer {token}"}, data=given_music, files=files)
        data = response.json()

        # Then
        assert response.status_code == 201
        assert data["title"] == given_music["music_title"]
        assert data["genre"] == given_music["music_genre"]
        assert given_music["music_title"].replace(
            " ", "_") in data["audio_file_name"]
        assert given_music["music_title"].replace(
            " ", "_") in data["cover_file_name"]
        assert data["published"] == False
    finally:
        for file in file_handles:
            file.close()


@pytest.mark.usefixtures("test_db")
def test_add_music_with_wrong_files_types():
    file_handles: list[BufferedReader] = []
    try:
        # Given
        files = {}
        i = 0
        for key, file in given_wrong_files.items():
            file_handle = open(file[1], "rb")
            file_handles.append(file_handle)
            files[key] = (file[0], file_handles[i], file[2])
            i += 1
        # When
        token = authenticate_util(client, given_artist)
        response = client.post(
            "/musics/", headers={"Authorization": f"Bearer {token}"}, data=given_music, files=files)
        data = response.json()

        # Then
        assert response.status_code == 400
        assert data["detail"]["audio_file"] == "Audio file must be mp3 or wav"
        assert data["detail"]["cover_file"] == "Cover file must be jpeg or png"
    finally:
        for file in file_handles:
            file.close()


@pytest.mark.usefixtures("test_db")
def test_get_all_musics():
    # Given
    music = seed_music(client, given_artist, given_music, given_files)

    # When
    response = client.get("/musics/")
    data = response.json()

    # Then
    assert response.status_code == 200
    assert len(data) == 1
    assert data[0]["title"] == music["title"]
    assert data[0]["genre"] == music["genre"]


@pytest.mark.usefixtures("test_db")
def test_stream_music_by_id():
    # Given
    music = seed_music(client, given_artist, given_music, given_files)

    # When
    response = client.get(f"/musics/{music['id']}")

    # Then
    assert response.status_code == 200
    assert response.headers["content-type"] in ["audio/mpeg", "audio/wav"]


@pytest.mark.usefixtures("test_db")
def test_publish_music():
    # Given
    music, token = seed_music(client, given_artist,
                              given_music, given_files, with_token=True)
    # When
    response = client.put(
        f"/musics/{music['id']}/publish", headers={"Authorization": f"Bearer {token}"})
    data = response.json()

    # Then
    assert response.status_code == 200
    assert data["title"] == given_music["music_title"]
    assert data["genre"] == given_music["music_genre"]
    assert data["published"] == True
    assert data["published_date"] is not None


@pytest.mark.usefixtures("test_db")
def test_publish_music_that_is_already_published():
    # Given
    music, token = seed_published_music(
        client, given_artist, given_music, given_files, with_token=True)

    # When
    response = client.put(
        f"/musics/{music['id']}/publish", headers={"Authorization": f"Bearer {token}"})
    data = response.json()

    # Then
    assert response.status_code == 400
    assert data["detail"] == "Music already published"


@pytest.mark.usefixtures("test_db")
def test_publish_music_not_owned_by_artist():
    # When
    music = seed_music(client, given_artist_2, given_music_2, given_files)
    token = authenticate_util(client, given_artist)
    response = client.put(
        f"/musics/{music['id']}/publish", headers={"Authorization": f"Bearer {token}"})
    data = response.json()

    # Then
    assert response.status_code == 403
    assert data["detail"] == "You are not allowed to publish this music"


@pytest.mark.asyncio
@pytest.mark.usefixtures("test_db")
async def test_get_latest_musics():
    # Given
    seed_published_music(client, given_artist, given_music, given_files)
    await asyncio.sleep(0.5)
    seed_published_music(client, given_artist_2, given_music_2, given_files)
    await asyncio.sleep(0.5)
    seed_published_music(client, given_artist_3, given_music_3, given_files)

    # When
    response = client.get("/musics/latest")
    data = response.json()

    # Then
    assert response.status_code == 200
    assert data[0]["title"] == given_music_3["music_title"]
    assert data[1]["title"] == given_music_2["music_title"]
    assert data[2]["title"] == given_music["music_title"]


@pytest.mark.usefixtures("test_db")
def test_get_current_artist_published_musics():
    # Given
    music, token = seed_published_music(
        client, given_artist, given_music, given_files, with_token=True)

    # When
    reponse = client.get(
        "/musics/me/true", headers={"Authorization": f"Bearer {token}"})
    data = reponse.json()

    # Then
    assert reponse.status_code == 200
    assert data[0]["title"] == music["title"]


@pytest.mark.usefixtures("test_db")
def test_get_current_artist_unpublished_musics():
    # Given
    music, token = seed_music(
        client, given_artist, given_music, given_files, with_token=True)

    # When
    reponse = client.get(
        "/musics/me/false", headers={"Authorization": f"Bearer {token}"})
    data = reponse.json()

    # Then
    assert reponse.status_code == 200
    assert data[0]["title"] == music["title"]


@pytest.mark.usefixtures("test_db")
def test_get_artist_musics():
    # Given
    music = seed_published_music(
        client, given_artist, given_music, given_files)
    token = authenticate_util(client, given_artist_2)
    print(music)

    # When
    response = client.get(
        f"/musics/artist/{music['owner']['id']}", headers={"Authorization": f"Bearer {token}"})
    data = response.json()

    # Then
    assert response.status_code == 200
    assert data[0]["title"] == music["title"]
