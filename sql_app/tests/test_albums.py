import pytest
from fastapi.testclient import TestClient

from ..core.database import get_db
from ..core.storage import Storage
from ..main import app
from .fixtures.albums import (given_album, given_album_cover_file,
                              given_wrong_album_cover_file)
from .fixtures.artists import given_artist
from .utils.artists import authenticate_util
# test_db is used to override the get_db dependency
from .utils.db import override_get_db, test_db
from .utils.storage import OverrideStorage

app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[Storage] = OverrideStorage

client = TestClient(app)


@pytest.mark.usefixtures("test_db")
def test_add_album():
    # When
    token = authenticate_util(client, given_artist)
    response = client.post(
        "/albums/", headers={"Authorization": f"Bearer {token}"}, data=given_album, files=given_album_cover_file)
    data = response.json()

    # Then
    assert response.status_code == 201
    assert data["title"] == given_album["album_title"]
    assert data["genre"] == given_album["album_genre"]
    assert given_album["album_title"].replace(
        " ", "_") in data["cover_file_name"]


@pytest.mark.usefixtures("test_db")
def test_add_album_with_wrong_cover_file_type():
    # When
    token = authenticate_util(client, given_artist)
    response = client.post(
        "/albums/", headers={"Authorization": f"Bearer {token}"}, data=given_album, files=given_wrong_album_cover_file)
    data = response.json()

    # Then
    assert response.status_code == 400
    assert data["detail"]["cover_file"] == "Cover file must be jpeg or png"
