from sqlalchemy.orm import Session

from . import models, schemas


def get_album(db: Session, album_id: int) -> schemas.Album:
    return db.query(models.Album).filter(models.Album.id == album_id).first()

def get_published_album(db: Session, album_id: int) -> schemas.Album:
    return db.query(models.Album).filter(models.Album.id == album_id, models.Album.published == True).first()


def get_albums(db: Session, skip: int = 0, limit: int = 100) -> list[schemas.Album]:
    return db.query(models.Album).offset(skip).limit(limit).all()


def create_album(db: Session, album_order: schemas.AlbumOrder, owner_id: int) -> schemas.Album:
    db_album = models.Album(title=album_order.title, cover_file_name=album_order.cover_file_name, genre=album_order.genre._value_,
                            owner_id=owner_id)

    db.add(db_album)
    db.commit()
    db.refresh(db_album)

    return db_album
