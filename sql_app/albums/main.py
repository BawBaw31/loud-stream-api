import pathlib

from fastapi import APIRouter, Depends, File, Form, UploadFile
from sqlalchemy.orm import Session

from ..artists.schemas import Artist
from ..core.database import get_db
from ..core.enums import GenresEnum
from ..core.security import get_current_artist
from ..core.storage import Storage
from ..core.utils import check_album_cover_file_type
from . import crud, schemas

router = APIRouter(
    prefix="/albums",
    tags=["albums"],
)


@router.post("/", response_model=schemas.Album, status_code=201)
async def add_album(cover_file: UploadFile = File(...), album_title: str = Form(...),
                    album_genre: GenresEnum = Form(...), db: Session = Depends(get_db),
                    storage: Storage = Depends(Storage), current_artist: Artist = Depends(get_current_artist)):

    await check_album_cover_file_type(cover_file)

    cover_file_name = storage.generate_file_name("cover", pathlib.Path(
        cover_file.filename).suffix, album_title, current_artist.stage_name)

    await storage.upload_file_to_s3(cover_file, cover_file_name)

    album_order = schemas.AlbumOrder(
        title=album_title, genre=album_genre, cover_file_name=cover_file_name)

    return crud.create_album(db, album_order, current_artist.id)
