from passlib.context import CryptContext
from sqlalchemy.orm import Session

from . import models, schemas


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_artist(db: Session, artist_id: int):
    return db.query(models.Artist).filter(models.Artist.id == artist_id).first()


def get_artist_by_email(db: Session, email: str):
    return db.query(models.Artist).filter(models.Artist.email == email).first()


def get_artists(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Artist).offset(skip).limit(limit).all()


def get_artists_by_query(db: Session, query: str, limit: int = 12):
    return db.query(models.Artist).filter(models.Artist.stage_name.like(f"%{query}%")).limit(limit).all()


def create_artist(db: Session, artist: schemas.ArtistOrder):
    db_artist = models.Artist(email=artist.email, stage_name=artist.stage_name,
                              hashed_password=pwd_context.hash(artist.password))
    db.add(db_artist)
    db.commit()
    db.refresh(db_artist)
    return db_artist
